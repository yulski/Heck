/**
 * Checks links in either HTML code or a whole directory. Sends GET request to all URLs, and 
 * looks for files to match each path link. Failed GET results in error, while no match for a path is 
 * just a warning. 
 * @module heck
 */

const htmljsparser = require('htmljs-parser');
const validUrl = require('valid-url');
const fs = require('fs');
const path = require('path');
const dns = require('dns');
const exec = require('child_process').exec;

let files = []; // TODO do something about this

// TODO less ugly and slow -_- ...

/**
 * Check the links in input. Input is either text or the path to a directory. If input is a directory, 
 * job must be 'directory'.
 * @param {string} input - either the HTML string, or a path to a directory
 * @param {string} job - the type of check to carry out. if 'directory' then input is understood to be a 
 * path to a directory.
 * @returns {Promise} Promise that resolves to a list of the error & warning messages generated
 */
module.exports = function (input, job = 'text') {
    let promises = [];

    return new Promise((fulfill, reject) => {
        if (job === 'directory') {
            // if directory, find all files in input dir
            findFiles(input).then(fileList => {
                files = fileList;
                let loops = 0;
                // loop through all files found
                for (let file of files) {
                    let filePath = path.join(input, file);
                    // run lstat on each file
                    fs.lstat(filePath, (err, stats) => {
                        if (err) console.error(err);
                        // check if file is a HTML file
                        if (stats.isFile()) {
                            let ext = path.extname(file);
                            if (ext === '.html' || ext === '.htm') {
                                // read file if its a HTML file
                                fs.readFile(filePath, {
                                    encoding: 'utf-8'
                                }, (err, data) => {
                                    if (err) console.error(err);
                                    let fileContent = data.toString();
                                    // create parser to parse the file contents
                                    let parser = htmljsparser.createParser({
                                        onOpenTag: (event) => {
                                            // <a> and <link> both have 'href' attribute
                                            if (event.tagName === 'a' || event.tagName === 'link') {
                                                for (let attr of event.attributes) {
                                                    if (attr.name === 'href') {
                                                        // get link lines
                                                        let lines = getLines(fileContent, event.pos, event.endPos);
                                                        let formattedEvent = {
                                                            tagName: event.tagName,
                                                            firstLine: lines.firstLine,
                                                            lastLine: lines.lastLine,
                                                            href: attr.literalValue,
                                                            file: file
                                                        };
                                                        // add Promise from event handling to promises array
                                                        promises.push(handleEvent(formattedEvent));
                                                        break;
                                                    }
                                                }
                                            } else if (event.tagName === 'img' || event.tagName === 'script') {
                                                // <img> and <script> both have 'src' attribute
                                                for (let attr of event.attributes) {
                                                    if (attr.name === 'src') {
                                                        // get link lines
                                                        let lines = getLines(fileContent, event.pos, event.endPos);
                                                        let formattedEvent = {
                                                            tagName: event.tagName,
                                                            firstLine: lines.firstLine,
                                                            lastLine: lines.lastLine,
                                                            href: attr.literalValue,
                                                            file: file
                                                        };
                                                        // add Promise from event handling to promises array
                                                        promises.push(handleEvent(formattedEvent));
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    });
                                    // parse the file contents
                                    parser.parse(fileContent);
                                    loops++;
                                    // if final loop, resolve all promises, filter out null messages, 
                                    // and fulfill with the remaining messages
                                    if (loops === files.length) {
                                        Promise.all(promises).then(messages => {
                                            messages = messages.filter(msg => msg);
                                            fulfill(messages);
                                        });
                                    }
                                });
                            } else {
                                // if not a HTML file, just proceed with loop
                                loops++;
                                if (loops === files.length) {
                                    Promise.all(promises).then(messages => {
                                        messages = messages.filter(msg => msg);
                                        fulfill(messages);
                                    });
                                }
                            }
                        } else {
                            // proceed with loop if its a directory
                            loops++;
                            if (loops === files.length) {
                                Promise.all(promises).then(messages => {
                                    messages = messages.filter(msg => msg);
                                    fulfill(messages);
                                });
                            }
                        }
                    });
                }
            });
        } else {
            // if not a directory job...

            // create parser to parse the HTML
            let parser = htmljsparser.createParser({
                onOpenTag: (event) => {
                    // <a> and <link> both have 'href' attribute
                    if (event.tagName === 'a' || event.tagName === 'link') {
                        for (let attr of event.attributes) {
                            if (attr.name === 'href') {
                                // get link lines
                                let lines = getLines(input, event.pos, event.endPos);
                                let formattedEvent = {
                                    tagName: event.tagName,
                                    firstLine: lines.firstLine,
                                    lastLine: lines.lastLine,
                                    href: attr.literalValue
                                };
                                // add Promise from event handling to promises array
                                promises.push(handleEvent(formattedEvent));
                                break;
                            }
                        }
                    } else if (event.tagName === 'img' || event.tagName === 'script') {
                        // <img> and <script> both have 'src' attribute
                        for (let attr of event.attributes) {
                            if (attr.name === 'src') {
                                // get link lines
                                let lines = getLines(input, event.pos, event.endPos);
                                let formattedEvent = {
                                    tagName: event.tagName,
                                    firstLine: lines.firstLine,
                                    lastLine: lines.lastLine,
                                    href: attr.literalValue
                                };
                                // add Promise from event handling to promises array
                                promises.push(handleEvent(formattedEvent));
                                break;
                            }
                        }
                    }
                }
            });
            // parse the input text
            parser.parse(input);

            // resolve all promises, filter out null messages, 
            // and fulfill with the remaining messages
            Promise.all(promises).then(messages => {
                messages = messages.filter(msg => msg);
                fulfill(messages);
            });
        }
    });
};

/**
 * The message resulting from a link check.
 */
class Message {

    /**
     * Create new message with specified params.
     * @param {string} type - the type of message (usually error or warning)
     * @param {int} startLine - the line number the link starts on
     * @param {int} endLine - the line number the link ends on
     * @param {string} message - the message body
     * @param {string} file - (optional) the file the Message concerns. used in directory jobs.
     */
    constructor(type, startLine, endLine, message, file = null) {
        this.type = type;
        this.startLine = startLine;
        this.endLine = endLine;
        this.message = message;
        if (file) {
            this.file = file;
        }
    }
}

/**
 * Get the line numbers for a link based on its overall character position.
 * @param {string} html - The entire HTML the link is found in.
 * @param {int} pos - the character position for the beginning of the link
 * @param {int} endPos - the character position for the end of the link
 * @returns {Object} object with keys firstLine and lastLine to indicate link start and end line numbers
 */
function getLines(html, pos, endPos) {
    // split HTML by newlines
    let lines = html.split('\n');
    let ret = {
        firstLine: -1,
        lastLine: -1
    };
    let total = 0;
    // loop through lines
    for (let i = 0; i < lines.length; i++) {
        // add line length to total
        total += lines[i].length;
        // if firstLine not set yet and total >= pos, the link must start 
        // on line 'i'
        if (total >= pos && ret.firstLine === -1) {
            ret.firstLine = i + 1;
        }
        // if lastLine not set yet and total >= endPos, the link must end 
        // on line 'i'
        if (total >= endPos && ret.lastLine === -1) {
            ret.lastLine = i + 1;
        }
    }
    // if lastLine is somehow < firstLine, set it to a sensible value
    if (ret.lastLine < ret.firstLine) {
        ret.lastLine = ret.firstLine + 1;
    }
    return ret;
}

/**
 * Get the type of link. Either 'internet' for URLs, 'path' for paths, or 'unknown'.
 * @param {string} href - the link 
 * @returns {string} the link type
 */
function getLinkType(href) {
    if (validUrl.isWebUri(href)) {
        return 'internet';
    } else if (isPath(href)) {
        return 'path';
    } else {
        return 'unknown';
    }
}

/**
 * Determine if a link is a path.
 * @param {string} href - the link
 * @returns {boolean} true if href is a path, false otherwise
 */
function isPath(href) {
    let regex = /^(.+)\/([^/]+)$/;
    return regex.test(href);
}

/**
 * Format a htmljs-parser event.
 * @param {Object} event - event generated by htmljs-parser
 * @param {Object} attr - attributes for a tag as parsed by htmljs-parser
 * @returns {Object} the formatted event
 */
function formatEvent(event, attr) {
    return {
        tagName: event.tagName,
        pos: event.pos,
        endPos: event.endPos,
        href: attr.literalValue
    };
}

/**
 * Handle a link.
 * @param {Object} event - appropriately formatted htmljs-parser event object 
 * @returns {Promise} a Promise that resolves to a message if an issue was detected with the link or null otherwise
 */
function handleEvent(event) {
    return new Promise((fulfill, reject) => {
        let href = event.href;
        // find link type
        let type = getLinkType(href);
        if (type === 'internet') {
            // handle internet link
            handleInternetLink(href).then(res => {
                let message = null;
                // if internet link fails, return an error
                if (!res) {
                    message = new Message('error', event.firstLine, event.lastLine, `The URL ${href} is not valid.`, event.file ? event.file : null);
                }
                fulfill(message);
            }).catch(err => {
                // if internet link causes error, return an error
                fulfill(new Message('error', event.firstLine, event.lastLine, `Experienced an error attempting to reach ${href}.`,
                    event.file ? event.file : null));
            });
        } else if (type === 'path') {
            let message = null;
            // if path, handle path link
            if (!handlePathLink(href)) {
                // if path fails, return warning. path could be some path configured by a local server or framework 
                // so unlike internet link, if it fails here it could still be valid
                message = new Message('warning', event.firstLine, event.lastLine, 
                    `The file at path ${href} could not be found.`, event.file ? event.file : null);
            }
            fulfill(message);
        } else {
            fulfill(null);
        }
    });
}

/**
 * Handle an internet link.
 * @param {string} link - the link
 * @returns {Promise} a Promise that resolves to true if the link could be reached with a ping, false otherwise
 */
function handleInternetLink(link) {
    return ping(link);
}

/**
 * Handle a path link.
 * @param {string} link - the link
 * @returns {boolean} true if the path was found in the project files and false otherwise
 */
function handlePathLink(link) {
    if (link.startsWith('/')) {
        link = link.slice(1);
    }
    if (link.endsWith('/')) {
        link = link.slice(0, link.length - 1);
    }
    return files.length > 0 && files.includes(link);
}

/**
 * Finds all files and subdirectories of a directory.
 * @param {string} dir - the directory path
 * @returns {Promise} a Promise that resolves to an array of files and directories found inside the directory
 */
function findFiles(dir) {
    let retFiles = [];
    let counter = 0;
    return new Promise((fulfill, reject) => {
        if (!dir) {
            fulfill(null);
        }
        // get dir stats
        fs.lstat(dir, (err, stats) => {
            // if its a file, fulfill with it as only array element
            if (stats.isFile()) {
                fulfill([dir]);
            } else if (stats.isDirectory()) {
                // if its a directory, find all fiels inside it (1 level down)
                fs.readdir(dir, (err, files) => {
                    // if some files found, loop through them
                    if (files != null && files.length > 0) {
                        for (let file of files) {
                            // lstat on each file
                            fs.lstat(path.join(dir, file), (err, stats) => {
                                // if its a file add it to array
                                if (stats.isFile()) {
                                    retFiles.push(file);
                                    counter++;
                                    if (counter == files.length) {
                                        fulfill(retFiles);
                                    }
                                } else if (stats.isDirectory()) {
                                    // if its a directory, recurse and find all files inside that directory
                                    findFiles(path.join(dir, file)).then((foundFiles) => {
                                        // if files found, add them w/ full path to array
                                        if (foundFiles) {
                                            let dirFoundFiles = foundFiles.map((f) => {
                                                return `${file}/${f}`;
                                            });
                                            retFiles = retFiles.concat(dirFoundFiles);
                                        }
                                        // also add the directory to array
                                        retFiles.push(file);
                                        // if final loop, fulfill promise
                                        counter++;
                                        if (counter == files.length) {
                                            fulfill(retFiles);
                                        }

                                    });
                                }
                            });
                        }
                    } else {
                        fulfill(null);
                    }
                });
            }
        });
    });
}

/**
 * Send a ping to a URL.
 * @param {string} url - The URL that the ping should be sent to.
 * @returns {Promise} A Promise that resolves to true if the ping response was successfull, false otherwise. The
 * Promise is rejected if the URL is invalid.
 */
function ping(url) {
    return new Promise((fulfill, reject) => {
        let domain = urlToDomain(url);

        dns.resolve(domain, (err, out) => {
            if(err || out.length === 0) {
                reject('Invalid URL ' + url);
            } else {
                let addr = out[0];
                exec(`ping ${addr}`, (err, out) => {
                    fulfill(!err);
                });
            }
        });
    });
}

/**
 * Converts a URL to a domain name. 
 * @param {string} url - The URL to be converted.
 * @returns {string} The domain name obtained from the URL.
 */
function urlToDomain(url) {
    let sliceIndex = 0;
    if(url.startsWith('http://')) {
        sliceIndex = 7;
    } else if(url.startsWith('https://')) {
        sliceIndex = 8;
    }

    let domain = url.slice(sliceIndex);

    return domain.slice(0, domain.indexOf('/'));
}
